# react-awesome-loading-spinner

> ReactJS 19 different loading spinner designs in one component

[![NPM](https://img.shields.io/npm/v/react-awesome-loading-spinner.svg)](https://www.npmjs.com/package/react-awesome-loading-spinner) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm i react-awesome-loading-spinner --save
```
## GitLab
https://gitlab.com/damjan89/react-awesome-loading-spinner
## Usage
React >= 16.9.0
```tsx

```

### Loading Spinner Themes (this.state.config.spinnerTheme)
```
```

## Preview
## License
Special Thanks to Vladimir Stepura for this post (all pure html & css can be found in url bellow) and all other developers/designers 
url: https://freefrontend.com/css-toggle-switches/
MIT © [Nick Dam](https://gitlab.com/damjan89)
