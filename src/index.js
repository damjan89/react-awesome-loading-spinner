"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var default_1 = require("./switches/default");
var radial_1 = require("./switches/radial");
var simple_1 = require("./switches/simple");
var oldSchool_1 = require("./switches/oldSchool");
var gender_1 = require("./switches/gender");
var bulb_1 = require("./switches/bulb");
var lamp_1 = require("./switches/lamp");
var lock_1 = require("./switches/lock");
var neon_1 = require("./switches/neon");
var zedDash_1 = require("./switches/zedDash");
var totoro_1 = require("./switches/totoro");
var elastic_1 = require("./switches/elastic");
var smiley_1 = require("./switches/smiley");
var rolling_1 = require("./switches/rolling");
var dayNight_1 = require("./switches/dayNight");
var invert_1 = require("./switches/invert");
var flat_1 = require("./switches/flat");
var neon2_1 = require("./switches/neon2");
var bear_1 = require("./switches/bear");
var components = {
    DefaultSwitch: default_1.default,
    RadialSwitch: radial_1.default,
    SimpleSwitch: simple_1.default,
    OldSchoolSwitch: oldSchool_1.default,
    GenderSwitch: gender_1.default,
    BulbSwitch: bulb_1.default,
    LampSwitch: lamp_1.default,
    LockSwitch: lock_1.default,
    NeonSwitch: neon_1.default,
    ZedDashSwitch: zedDash_1.default,
    TotoroSwitch: totoro_1.default,
    ElasticSwitch: elastic_1.default,
    SmileySwitch: smiley_1.default,
    RollingSwitch: rolling_1.default,
    DayNightSwitch: dayNight_1.default,
    InvertSwitch: invert_1.default,
    FlatSwitch: flat_1.default,
    Neon2Switch: neon2_1.default,
    BearSwitch: bear_1.default
};
var AwesomeToggleSwitch = /** @class */ (function (_super) {
    __extends(AwesomeToggleSwitch, _super);
    function AwesomeToggleSwitch(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            val: props.value ? props.value : false,
            configuration: props.config ? props.config : {
                toggleTheme: 'RadialSwitch',
                title: 'Title',
                leftText: 'On',
                rightText: 'Off'
            }
        };
        return _this;
    }
    AwesomeToggleSwitch.prototype.componentDidMount = function () { };
    AwesomeToggleSwitch.prototype.valChanged = function (e) {
        this.props.onChange(e);
        this.setState({
            val: e
        });
    };
    AwesomeToggleSwitch.prototype.render = function () {
        var _this = this;
        var name = this.state.configuration ? this.state.configuration.toggleTheme : 'DefaultSwitch';
        var component = components[name];
        return (React.createElement("div", null,
            React.createElement("p", null, this.state.val.toString()),
            React.createElement(component, { value: this.state.val, onValueChange: function (e) { return _this.valChanged(e); }, config: this.state.configuration })));
    };
    return AwesomeToggleSwitch;
}(React.Component));
exports.default = AwesomeToggleSwitch;
//# sourceMappingURL=index.js.map